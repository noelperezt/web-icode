import unicodedata

def crear_url(s):
    urlx = ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
    urlx = urlx.replace(' ', '-')
    return urlx.lower()