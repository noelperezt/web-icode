from .views import *
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', Inicio_blog, name='inicio_blog'),
    url(r'^compartir/$', SumarCompartidos, name='compartir_rrss'),
    url(r'^([a-z\-\_]+)/$', DetalleArticulo, name='detalle_articulo'),
    url(r'^categoria/([a-z\-\_]+)/$', LisatadoArticulo, name='listado_articulo'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)