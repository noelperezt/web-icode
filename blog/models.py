from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from froala_editor.fields import FroalaField

# Create your models here.

class Categorias(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    categoria = models.CharField(max_length=60, verbose_name='Nombre Categoria', db_index=True, unique=True, null=False,
                                 blank=False)
    url = models.CharField(max_length=120, verbose_name='Url de la Categoria', null=True, blank=True, db_index=True)
    estado = models.CharField(max_length=1, choices=STATUS, default='A')
    total_articulos = models.IntegerField(default=0, verbose_name='Cantidad de articulos')
    meta_descripcion = models.CharField(max_length=255, null=True, verbose_name='Meta Descripcion')
    meta_titulo = models.CharField(max_length=120, null=True,verbose_name='Meta Titulo')

    class Meta:
        db_table = 'tb_blog_categoria'
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.categoria

class Post(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    titulo = models.CharField(max_length=120, verbose_name='Titulo Articulo')
    url = models.CharField(max_length=240, verbose_name='Url')
    fecha_creacion = models.DateField(default=timezone.now, editable=False, verbose_name='Fecha de creacion')
    descripcion = FroalaField()
    resumen = models.TextField(max_length=800, null=True, blank=True, verbose_name='Resumen')
    cantidad_megusta = models.IntegerField(default=0, verbose_name='Cantidad de Me gusta')
    compartido_tw = models.IntegerField(default=0, verbose_name='Compartido en Twitter')
    compartido_fb = models.IntegerField(default=0, verbose_name='Compartido en Facebook')
    compartido_gp = models.IntegerField(default=0, verbose_name='Compartido en Google Plus')
    ultima_lectura = models.DateTimeField(editable=False, verbose_name='Ultima lectura', null=True, blank=True)
    foto_principal = models.FileField(upload_to='documents/')
    foto_miniatura = models.CharField(max_length=200, null=False, blank=False)
    activo = models.BooleanField(default=True)
    cantidad_visitas = models.IntegerField(default=0, verbose_name='Cantidad de Visitas')
    id_usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    id_categoria = models.ForeignKey(Categorias, on_delete=models.PROTECT)

    class Meta:
        db_table = 'tb_blog_post'
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.titulo
class Usuario(models.Model):
    nombre = models.CharField(max_length=60)
    correo = models.CharField(max_length=30,
                              unique=True,
                              help_text=_('Necesario. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ only.'),
                              validators=[
                                  validators.RegexValidator(
                                      r'^[\w.@+-]+$',
                                      _('Ingrese un nombre de usuario válido. Este valor sólo puede contener '
                                        'Letras y números ' 'y @/./+/-/_ caracteres.')
                                  ),
                              ],
                              error_messages={
                                  'unique': _("Ya está registrado la dirección de correo."),
                              }, )
    activo = models.BooleanField(default=False)
    fecha_registro = models.DateField(default=timezone.now, editable=False, verbose_name='Fecha de creacion')
    class Meta:
        db_table = 'tb_blog_usuario'
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

class Comentarios(models.Model):
    comentario = models.CharField(max_length=600, verbose_name='Comentario realizado')
    id_usuario_comenta = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    id_articulo = models.ForeignKey(Post, on_delete=models.CASCADE)
    fecha_comentario = models.DateField(default=timezone.now, editable=False , verbose_name= 'Fecha del comentario')
    class Meta:
        db_table = 'tb_blog_comentario'
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'
