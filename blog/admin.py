from django.contrib import admin
from django.utils.datastructures import MultiValueDictKeyError
from blog.util.File import File
from blog.util.Functions import crear_url
from .models import Categorias, Post
from django.contrib.auth.models import User

# Register your models here.

class AdminCategorias(admin.ModelAdmin):
    list_filter = ('estado',)
    fieldsets = [
         (None, { 'fields': ['categoria', 'estado'] }),
         ('SEO', { 'fields': [ ('meta_titulo','meta_descripcion' )] } )
         ]
    list_display = ('categoria', 'total_articulos','meta_titulo')

    def save_model(self, request, obj, form, change):
        obj.categoria = request.POST['categoria']
        obj.meta_descripcion = request.POST['meta_descripcion']
        obj.meta_titulo = request.POST['meta_titulo']
        obj.url = crear_url(request.POST['categoria'])
        obj.save()

admin.site.register(Categorias, AdminCategorias)

class AdminPost(admin.ModelAdmin):
    list_filter = ('id_usuario','id_categoria', 'activo')
    fieldsets = [
        (None, {'fields': [('titulo')]}),
        (None, {'fields': [('foto_principal','id_categoria', 'activo')]}),
        (None, {'fields': ['resumen']}),
        (None,{'fields': ['descripcion']}),
    ]
    list_display = ('titulo', 'url','fecha_creacion','cantidad_visitas','compartido_tw','compartido_fb',
                    'compartido_gp','ultima_lectura',)

    def save_model(self, request, obj, form, change):
        try:
            filex = File(request.FILES['foto_principal'], '', 'images')
            filex.upload()
            obj.foto_principal = str(filex.getFile())
            obj.foto_miniatura = 'images/md_' +filex.getNameEncryp()
        except MultiValueDictKeyError:
            pass
        obj.titulo = request.POST['titulo']
        obj.url = crear_url(request.POST['titulo'])
        obj.resumen = request.POST['resumen']
        obj.descripcion = request.POST['descripcion']
        obj.id_usuario = User.objects.get(id=request.user.id)
        obj.save()

        #sumo 1 post mas a la cantidad de publicados en la categoria
        if not change:
            datos_categoria = Categorias.objects.get(id=request.POST['id_categoria'])
            cantidad = datos_categoria.total_articulos + 1
            Categorias.objects.filter(id=request.POST['id_categoria']).update(total_articulos=cantidad)

admin.site.register(Post, AdminPost)