from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Post, Categorias
import json
from django.contrib.auth.models import User
# Create your views here.

@csrf_exempt
def SumarCompartidos(request):
    try:
        datos_post = Post.objects.get(id=request.POST['post'])
        if request.POST['tipo_rrss'] == 'fb':
            cantidad = datos_post.compartido_fb + 1
            Post.objects.filter(id=request.POST['post']).update(compartido_fb=cantidad)

        elif request.POST['tipo_rrss'] == 'tw':
            cantidad = datos_post.compartido_fb + 1
            Post.objects.filter(id=request.POST['post']).update(compartido_tw=cantidad)

        elif request.POST['tipo_rrss'] == 'gp':
            cantidad = datos_post.compartido_gp + 1
            Post.objects.filter(id=request.POST['post']).update(compartido_gp=cantidad)

        return HttpResponse(json.dumps({'message': 'bien'}), content_type="application/json")
    except ObjectDoesNotExist:
        return HttpResponse(json.dumps({'mensaje': 'post no existe'}), content_type="application/json")

def Inicio_blog(request):
    '''

    :param request:
    :return:
    '''

    '''posts = Post.objects.filter(activo=True).select_related('id_usuario__id').\
        exclude(id_categoria=Categorias.objects.get(url='noticias')).order_by('-fecha_creacion')'''
    posts = Post.objects.filter(activo=True).select_related('id_usuario__id','id_categoria').order_by('-fecha_creacion')

    categorias = Categorias.objects.filter(estado='A')
    context = {
        'titulo': _('iCode - Notícas de programación y tecnología, cursos, ejemplos de código y más '),
        'descripcion': _('Blog de sistemas icode, para tenerse informado de los acontecimientos en el mundo de la '
                         'tecnolgía, robotica, redes sociales'),
        'posts': posts,
        'categorias': categorias
    }

    return render(request, 'inicio_blog.html', context)

def DetalleArticulo(request, url_articulo):
    datos_post = Post.objects.get(url=url_articulo, activo=True)

    #--------- actualiza la cantidad de visitas
    cantidad_visitas = datos_post.cantidad_visitas + 1
    Post.objects.filter(id=datos_post.id).update(ultima_lectura=timezone.now(), cantidad_visitas=cantidad_visitas)
    #-------------------------------------

    mas_post = Post.objects.filter(activo=True).exclude(url=url_articulo)[:5]
    categorias = Categorias.objects.filter(estado='A')
    context = {
        'titulo': _('iCode - ')+datos_post.titulo,
        'descripcion': datos_post.resumen,
        'meta_imagen' : datos_post.foto_principal,
        'meta_descripcion' : datos_post.resumen,
        'meta_url' : 'post/'+datos_post.url+'/',
        'post': datos_post,
        'mas_post': mas_post,
        'categorias': categorias,
    }

    return render(request, 'detalle_blog.html', context)


def LisatadoArticulo(request, url_categoria):
    categoria = Categorias.objects.get(url=url_categoria)
    mas_post = Post.objects.filter(activo=True).exclude(id_categoria=categoria).order_by('-fecha_creacion')[:5]

    datos_post = Post.objects.filter(activo=True, id_categoria=categoria).\
        select_related('id_usuario__id').order_by(
        '-fecha_creacion')

    paginator = Paginator(datos_post, 2)
    page = request.GET.get('page')
    try:
        datos = paginator.page(page)
    except PageNotAnInteger:
        datos = paginator.page(1)
    except EmptyPage:
        datos = paginator.page(paginator.num_pages)


    categorias = Categorias.objects.filter(estado='A')
    context = {
        'titulo': _('iCode - ') + categoria.categoria,
        'nombre_categoria' : categoria.categoria,
        'posts': datos,
        'mas_post' : mas_post,
        'categorias': categorias,
    }
    return render(request, 'listado_blog.html', context)