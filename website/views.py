from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMessage
from .forms import FormSuscripcion
# Create your views here.


def index(request):
    context = {
        'titulo': _('iCode - Creamos soluciones tecnológicas reales'),
        'descripcion': _('Sistemas iCode se dedica a desarrollar de sistemas, páginas web, aplicaciones moviles, '
                         'integraciones bancarias para ecommers'),
        'form_suscripion': FormSuscripcion
    }
    return render(request, 'inicio.html', context)

def nosotros(request):
    context = {
        'titulo': _('Conoce más acerca de nosotros iCode - Creamos soluciones tecnológicas reales'),
        'descripcion': _('iCode a brindar soluciones tecnologícas, conformada por un equipo de jovenes profesionales '
                         'dedicado a un área de experticia especifica unidos por el sentir de llevar a buenos términos '
                         'software de alta calidad provocando clientes satisfechos'),
        'form_suscripion': FormSuscripcion
    }
    return render(request, 'nosotros.html', context)

def servicios(request):
    return render(request, 'servicios.html',{'titulo' : _('Desarrollo tecnológicos de alto alcance.')})

def contacto(request):
    return render(request, 'contacto.html',{'titulo' : _('Solicitud de presupuesto y/o sugerenias.')})

def suscripcion_boletion(request):
    if request.method == 'POST':
        retorno = request.POST['url']
        form = FormSuscripcion(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.WARNING, _('Se ha registrado con éxito para recibir los boletines '
                                                              'informativos de Sistemas iCode'))
            return HttpResponseRedirect(retorno)
        else:
            messages.add_message(request, messages.WARNING, form.errors)

def correo_contacto(request):
    subject = request.POST['asunto']
    html_content = 'El usuario <strong>'+request.POST['nombre']+'</strong> sumistro el correo: ' \
                    '<strong>'+request.POST['correo']+'</strong> con el siguiente mensaje '+request.POST['mensaje']

    msg = EmailMessage(subject, html_content,'noreply@icode.com.ve',['programador.angel@gmail.com'])
    msg.content_subtype = "html"
    msg.send()
    messages.add_message(request, messages.WARNING, _("Mensaje enviado con éxito"))
    return HttpResponseRedirect('/contacto/')