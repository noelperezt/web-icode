from .views import *
from django.conf.urls import url

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^somos-icode/$', nosotros, name='nosotros'),
    url(r'^servicios/$', servicios, name='servicios'),
    url(r'^contacto/$', contacto, name='contacto'),
    url(r'^contacto/enviar-correo/$', correo_contacto, name='correo_contacto'),
    url(r'^suscripcion/$', suscripcion_boletion, name='suscripcion_boletion'),

]
