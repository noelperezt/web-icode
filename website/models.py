from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core import validators
# Create your models here.

class Suscripcion(models.Model):
    correo = models.CharField(max_length=30,
        unique=True,
        help_text=_('Necesario. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Ingrese un nombre de usuario válido. Este valor sólo puede contener '
                  'Letras y números ' 'y @/./+/-/_ caracteres.')
            ),
        ],
        error_messages={
            'unique': _("Ya está registrado la dirección de correo."),
        },)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'tb_icode_suscripcion'
        verbose_name = 'Suscripcion'
        verbose_name_plural = 'Suscripcions'
