from django import forms
from .models import Suscripcion

class FormSuscripcion(forms.ModelForm):
    class Meta:
        model = Suscripcion
        fields = ['correo']